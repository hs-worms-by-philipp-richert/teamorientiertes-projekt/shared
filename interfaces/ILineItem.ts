import { IProductItem } from './IProductItem';

export interface ILineItem {
  OrderId: bigint;
  LineItemId?: bigint;
  ItemId: bigint;
  Item?: IProductItem;
  StartDate: Date;
  EndDate: Date;
  ReturnDate: Date | undefined | null;
}
