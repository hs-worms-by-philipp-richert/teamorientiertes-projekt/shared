import { IRole } from './IRole';

export interface IUserRole {
  UserId: string;
  RoleId: number;
  Role?: IRole;
}
