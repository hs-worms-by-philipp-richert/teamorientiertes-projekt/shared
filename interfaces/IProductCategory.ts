import { ICategory } from './ICategory';
import { IProduct } from './IProduct';

export interface IProductCategory {
  ProductId: bigint;
  CategoryId: bigint;
  Category?: ICategory;
  Products?: IProduct[];
}
