import { OrderStatus } from '../enums/order-status.enum';
import { ILineItem } from './ILineItem';
import { IUser } from './IUser';

export interface IOrder {
  OrderId?: bigint;
  UserId: string;
  User?: IUser;
  OrderDate: Date;
  OrderStatus: OrderStatus;
  LineItems: ILineItem[];
}
