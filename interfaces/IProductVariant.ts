import { IProductVariantProperty } from './IProductVariantProperty';

export interface IProductVariant {
  ProductId: bigint;
  VariantId?: bigint;
  Description: string;
  Properties?: IProductVariantProperty[];
}
