import { IProductCategory } from './IProductCategory';
import { IProductImage } from './IProductImage';
import { IProductItem } from './IProductItem';
import { IProductManual } from './IProductManual';
import { IProductVariant } from './IProductVariant';

export interface IProduct {
  Name: string;
  ProductId?: bigint;
  IsInternal: boolean;
  RentingFee: number;
  Description: string;
  Variants?: IProductVariant[];
  Manufacturer: string;
  ProductItems?: IProductItem[];
  ProductImages?: IProductImage[];
  ProductManuals?: IProductManual[];
  Categories?: IProductCategory[];
}
