import { ILineItem } from './ILineItem';
import { IProduct } from './IProduct';
import { IProductVariant } from './IProductVariant';
import { IPsaCheck } from './IPsaCheck';
import { IWarehouseStorageTray } from './IWarehouseStorageTray';

export interface IProductItem {
  ItemId?: bigint;
  QrCode: string;
  Serialnumber: string | undefined;
  ProductId: bigint;
  Product?: IProduct;
  VariantId?: bigint;
  Tray: IWarehouseStorageTray;
  TrayId?: bigint;
  ManufacturingDate: Date | undefined;
  ExpiresInYears: number | undefined;
  IsPsa: boolean;
  PSA?: IPsaCheck[]
  IsDefect: boolean;
  Archived: boolean;
  Variant?: IProductVariant;
  LineItems?: ILineItem[];
}
