export interface CRUD {
  list(take: number, page: number): Promise<any>;

  listFiltered(take: number, page: number, filterOptions: any): Promise<any>;

  create(resource: any): Promise<any>;

  putById(id: any, resource: any): Promise<boolean>;

  readById(id: any): Promise<any>;

  deleteById(id: any): Promise<boolean>;

  patchById(id: any, resources: any): Promise<boolean>;
}
