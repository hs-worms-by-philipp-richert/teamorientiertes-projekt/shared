export interface IProductVariantProperty {
  PropertyId: bigint;
  VariantId: bigint;
  Name: string;
}
