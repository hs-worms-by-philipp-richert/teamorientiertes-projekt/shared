import { IOrder } from './IOrder';
import { IRole } from './IRole';

export interface IUser {
  Firstname: string;
  Lastname: string;
  UserId?: string;
  Street: string;
  Postal: string;
  City: string;
  Housenumber: string;
  Country: string;
  Email: string;
  Phone: string;
  Password?: string;
  HasRequestedRoleUpdate?: boolean;
  IsVerified: boolean;
  Orders?: IOrder[];
  RoleId: number;
  Role: IRole;
}
