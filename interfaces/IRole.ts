import { Permission } from '../enums/permissions.enum';

export interface IRole {
  RoleId?: number;
  Name: string;
  Permission: Permission;
}
