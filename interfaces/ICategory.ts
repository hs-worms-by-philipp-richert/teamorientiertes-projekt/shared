export interface ICategory {
  Name: string;
  CategoryId?: bigint;
  ChildCategories?: ICategory[];
  ParentCategoryId?: ICategory;
}
