import { IWarehouseRegion } from './IWarehouseRegion';

export interface IWarehouse {
  WarehouseId?: bigint;
  Name: string;
  Street: string;
  Postal: string;
  City: string;
  Housenumber: string;
  Country: string;
  Regions?: IWarehouseRegion[];
}
