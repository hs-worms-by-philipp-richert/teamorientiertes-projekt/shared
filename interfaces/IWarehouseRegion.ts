import { IWarehouse } from './IWarehouse';
import { IWarehouseStorage } from './IWarehouseStorage';

export interface IWarehouseRegion {
  RegionId?: bigint;
  WarehouseId: bigint;
  Warehouse?: IWarehouse;
  Name: string;
  Storages?: IWarehouseStorage[];
}
