import { IWarehouseRegion } from './IWarehouseRegion';
import { IWarehouseStorageTray } from './IWarehouseStorageTray';

export interface IWarehouseStorage {
  StorageId?: bigint;
  RegionId: bigint;
  Region?: IWarehouseRegion;
  Name: string;
  Trays?: IWarehouseStorageTray[];
}
