import { IWarehouseStorage } from './IWarehouseStorage';

export interface IWarehouseStorageTray {
  TrayId?: bigint;
  StorageId: bigint;
  Storage?: IWarehouseStorage;
  Name: string;
}
