export interface IProductManual {
  ProductId: bigint;
  ManualId: string;
  Name: string;
}
