import { IProductItem } from './IProductItem';

export interface IPsaCheck {
  PsaId: string;
  ItemId: bigint;
  Item?: IProductItem;
  DateChecked: Date;
}