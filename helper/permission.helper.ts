import { Permission } from "../enums/permissions.enum";

export default class PermissionHelper {
  static parsePermissions(octalValue: number | undefined): Permission[] {
    const permissions: Permission[] = [];

    if (octalValue == undefined)
      return permissions;

    permissions.push(Permission.Member);

    switch (octalValue) {
      case 7:
        permissions.push(Permission.InternalMember);
        permissions.push(Permission.WarehouseWorker);
        permissions.push(Permission.Admin);
        break;
      case 6:
        permissions.push(Permission.WarehouseWorker);
        permissions.push(Permission.Admin);
        break;
      case 5:
        permissions.push(Permission.InternalMember);
        permissions.push(Permission.Admin);
        break;
      case 4:
        permissions.push(Permission.Admin);
        break;
      case 3:
        permissions.push(Permission.InternalMember);
        permissions.push(Permission.WarehouseWorker);
        break;
      case 2:
        permissions.push(Permission.WarehouseWorker);
        break;
      case 1:
        permissions.push(Permission.InternalMember);
        break;
    }

    return permissions;
  }

  static sumPermissions(permissionList: Permission[]): number {
    let summe = 0;
    for (let i in permissionList) {
      summe += parseInt(i);
    }

    return summe;
  }

  static isAllowedTo(permission: Permission, userPermissions: Permission[]): boolean {
    return userPermissions.includes(permission);
  }
}