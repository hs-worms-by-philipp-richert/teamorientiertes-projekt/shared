export enum DateComparison {
  OnlyValid = 0,
  Future = 1,
  TodayAndFuture = 2
}