export enum OrderStatus {
  Open = 0,
  Accepted = 1,
  ReadyForPickup = 2,
  InRental = 3,
  Completed = 4,
  Archived = 5,
  Canceled = 6,
}
