export enum Permission {
  Member = 0,
  InternalMember = 1,
  WarehouseWorker = 2,
  Admin = 4,
}
