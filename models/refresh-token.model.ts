export interface RefreshToken {
  sessionId: string;
  tokenId: string;
  userId: string;
}
