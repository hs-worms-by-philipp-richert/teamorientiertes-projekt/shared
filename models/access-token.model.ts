import { IUser } from '../interfaces/IUser';

export interface AccessToken {
  user: IUser;
  sessionId: string;
  associatedRefreshToken: string;
}
