export interface ProductListFilter {
  search: string;
  categories: Array<string>;
  manufacturer: string;
  sort: 'ASC' | 'DESC' | undefined | string;
  sortBy: string;
  isInternal: boolean;
  startDate: string;
  endDate: string;
}
