import { OrderStatus } from '../enums/order-status.enum';

export interface OrderFilterList {
  search: string;
  sort: 'ASC' | 'DESC' | string | undefined;
  orderDate: string;
  orderStatus: OrderStatus | undefined;
}
