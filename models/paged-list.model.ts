export interface PagedList<T> {
  currentPage: number;
  totalLength: number;
  list: T;
}

export interface PagedAvailabilityList<T> {
  currentPage: number;
  totalAvailable: number;
  totalUnavailable: number;
  listAvailable: T;
  listUnavailable: T;
}

export interface PagedListCategory<T, K> {
  category: K;
  subCatgories: K[];
  pagedList: PagedList<T>;
}
